import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  registerUserData: any = {};

  constructor( private auth: AuthService , private router: Router ){}

  ngOnInit() {
  }

  registerUser( e: Event ){
    
    e.preventDefault();

    this.auth.registerUser( this.registerUserData ).subscribe( 
      res => {
        localStorage.setItem( 'token' , res.token );
        this.router.navigate( [ '/special' ] );
      } 
      , err => console.log( "err:", err ) );
  }

}
