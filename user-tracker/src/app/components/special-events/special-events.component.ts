import { Component, OnInit } from '@angular/core';
import { EventService } from 'src/app/services/event.service';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-special-events',
  templateUrl: './special-events.component.html',
  styleUrls: ['./special-events.component.scss']
})
export class SpecialEventsComponent implements OnInit {

  specialEvents: any = [];

  constructor( private eventService: EventService, private router: Router ){}

  ngOnInit() {
    this.getSpecEvts();
  }

  getSpecEvts(){
    this.eventService.getSpecialEvents().subscribe(
      res => this.specialEvents = res
      , err => {
        if( err instanceof HttpErrorResponse ){
          if( err.status === 401 ){
            this.router.navigate( [ '/login' ] );
          }
        }
      }
    );
  }

}
