import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginUserData: any = {};

  constructor( private auth: AuthService , private router: Router ){}

  ngOnInit() {
  }

  loginUser( e: Event ){
    
    e.preventDefault();

    this.auth.loginUser( this.loginUserData ).subscribe(
      res => {
        localStorage.setItem( 'token' , res.token );
        this.router.navigate( [ '/special' ] );
      }
      , err => console.log( "err: ", err )
    );
  }

}
