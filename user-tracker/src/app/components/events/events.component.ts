import { Component, OnInit } from '@angular/core';
import { EventService } from 'src/app/services/event.service';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.scss']
})
export class EventsComponent implements OnInit {

  events: any = [];

  constructor( private eventService: EventService ){}

  ngOnInit() {
    this.getEvts();
  }

  getEvts(){
    this.eventService.getEvents().subscribe(
      res => this.events = res
      , err => console.log( "err: ", err )
    );
  }

}
