import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EventService {

  private eventsUrl: string = "http://localhost:4000/api/events";

  private specialEventsUrl: string = "http://localhost:4000/api/special";

  constructor( private http: HttpClient ){}

  getEvents(){
    return this.http.get<any>( this.eventsUrl );
  }

  getSpecialEvents(){
    return this.http.get<any>( this.specialEventsUrl );
  }

}
