import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';

import { NavComponent } from './components/elements/nav/nav.component';

import { EventsComponent } from './components/events/events.component';
import { SpecialEventsComponent } from './components/special-events/special-events.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';

import { TokenInterceptorService } from './services/token-interceptor.service';

@NgModule({
  declarations: [
    AppComponent
    , NavComponent
    , EventsComponent
    , SpecialEventsComponent
    , LoginComponent
    , RegisterComponent
  ],
  imports: [
    BrowserModule
    , AppRoutingModule
    , FormsModule
    , HttpClientModule
  ],
  providers: [
    { 
      provide: HTTP_INTERCEPTORS 
      , useClass: TokenInterceptorService
      , multi: true 
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
