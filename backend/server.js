// =============================================
// process.env SETUP
// =============================================
const { PORT } = process.env;

// new boilerplate
// =============================================
const cors = require("cors");
const bodyParser = require("body-parser");

// BASE SETUP
// =============================================
const express = require("express");
const app = express();
const apiRouter = require("./routes/api");

app.use( cors() );
app.use( bodyParser.json() );

app.use( '/api' , apiRouter ); 
app.listen( PORT , () => { 
  // console.log( "process.env.CONNECTION_URL" , process.env.CONNECTION_URL );
  console.log( `Server is running on port ${PORT}` );   
} );