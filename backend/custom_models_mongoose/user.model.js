const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let UserSchema = new Schema( {
  email: { type: String }
  , password: { type: String }
} );

module.exports = mongoose.model( "User", UserSchema );