// =============================================
// process.env SETUP
// =============================================
const { CONNECTION_URL , JWT_SECRET } = process.env;

// BASE SETUP
// =============================================
const express = require("express");
const apiRouter = express.Router();
const mongoose = require("mongoose");
const jwt = require('jsonwebtoken');

// MODEL DEFINITION
// =============================================
const User = require('../custom_models_mongoose/user.model');
const Event = require('../custom_models_mongoose/event.model');
const Specialevent = require('../custom_models_mongoose/specialevent.model');

// DB CONNECTION
// =============================================
mongoose.connect( CONNECTION_URL , { useNewUrlParser: true , useUnifiedTopology: true, useFindAndModify: false } );

const connection = mongoose.connection;
connection.once( "open" , () => { console.log( "MongoDB database connection established successfully" ); } );

// TEMPORARY
const verifyToken = (req, res, next ) => {
  try {

    if(!req.headers.authorization) {
      return res.status(401).send('Unauthorized request');
    }

    let token = req.headers.authorization.split(' ')[1];
    if(token === 'null') {
      return res.status(401).send('Unauthorized request');
    }

    let payload = jwt.verify(token, JWT_SECRET );

    if(!payload) {
      return res.status(401).send('Unauthorized request');
    }

    console.log( "payload" , payload );
    console.log( "payload.custom_subject" , payload.custom_subject );
    req.userId = payload.custom_subject;

    next();

  } catch( error ){
    
    return res.status(403).send('No token provided');

  }
};
// =============================================

// ROUTING ASSIGNMENTS
// =============================================
apiRouter.get( '/events' , ( req, res ) => {
  
  Event.find( ( err, events ) => {
    if(err){
      console.log( "err" , err );
    }
    res.json( events );
  } );

} );

apiRouter.get( '/special' , verifyToken, ( req, res ) => {
  
  Specialevent.find( ( err, specialevents ) => {
    if(err){
      console.log( "err" , err );
    }
    res.json( specialevents );
  } );

} );

apiRouter.post( '/register' , ( req, res ) => {
  let userData = req.body;
  let user = new User( userData );
  
  user.save( ( err, registeredUser ) => {
    if(err){
      console.log( "err", err );
    }
    let payload = { custom_subject: registeredUser._id };
    let token = jwt.sign( payload, JWT_SECRET );
    res.status(200).send( { token } );
  } );

} );

apiRouter.post( '/login' , ( req, res ) => {
  
  let userData = req.body;
  let paramObj = { email: userData.email };

  User.findOne( paramObj , ( err, user ) => {
    if(err){
      console.log( "err" , err );
    }
    if(!user) {
      res.status(401).send('Invalid Email');
    }
    else
    if( user.password !== userData.password ){
      res.status(401).send('Invalid Password');
    }
    else {
      let payload = { custom_subject: user._id };
      let token = jwt.sign( payload, JWT_SECRET );
      res.status(200).send( { token } );
    }
  } );

} );

module.exports = apiRouter;